<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::match(['get', 'post'],'/home', 'HomeController@index')->name('home');
Route::match(['get', 'post'], '/user', 'UserController@index');
Route::match(['get', 'post'], '/user/update/{user}', 'UserController@update');

Route::match(['get', 'post'], '/setting', 'SettingController@index');
Route::match(['get', 'post'], '/setting/create', 'SettingController@create');
Route::match(['get', 'post'], '/setting/update', 'SettingController@update');
Route::match(['get', 'post'], '/setting/api', 'SettingController@api');

Route::match(['get', 'post'], '/purses-control', 'PursesControlController@index');
Route::match(['get', 'post'], '/purses-control/create', 'PursesControlController@create');
Route::match(['get', 'post'], '/purses-control/delete', 'PursesControlController@delete');
Route::match(['get', 'post'], '/purses-control/update/{purse}', 'PursesControlController@update');

Route::get('/purses', 'PursesViewController@index');
Route::get('/purses/create', 'PursesViewController@showCreate');
Route::get('/purses/delete', 'PursesViewController@showDelete');
Route::match(['get', 'post'], '/purses/update/', 'PursesViewController@showUpdate');
Route::match(['get', 'post'], '/purses/select', 'PursesViewController@select');

Route::match(['get', 'post'], '/transactions-control', 'TransactionsControlController@index');
Route::match(['get', 'post'], '/transactions-control/create', 'TransactionsControlController@create');
Route::match(['get', 'post'], '/transactions-control/delete/{transaction}', 'TransactionsControlController@delete');
Route::match(['get', 'post'], '/transactions-control/update/{transaction}', 'TransactionsControlController@update');

Route::get('/transactions', 'TransactionsViewController@index');
Route::match(['get', 'post'], '/transactions/create', 'TransactionsViewController@showCreate');
//Route::get('/transactions/delete', 'TransactionsViewController@delete');
Route::match(['get', 'post'], '/transactions/update/{transaction}', 'TransactionsViewController@showUpdate');

Route::get('/send', 'MailController@send');
