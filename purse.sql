-- Adminer 4.7.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `currencies`;
CREATE TABLE `currencies` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ccy` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `currencies` (`id`, `name`, `ccy`, `created_at`, `updated_at`) VALUES
(840,	'доллар США',	'USD',	NULL,	NULL),
(978,	'Евро',	'EUR',	NULL,	NULL),
(980,	'гривна',	'UAH',	NULL,	NULL);

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1,	'2014_10_12_000000_create_users_table',	1),
(4,	'2014_10_12_100000_create_password_resets_table',	2),
(5,	'2019_08_19_000000_create_failed_jobs_table',	2),
(6,	'2020_05_29_192736_create_currencies_table',	3),
(7,	'2020_05_29_201939_create_users_settings_table',	4),
(8,	'2020_05_29_205024_create_table_purses',	5),
(9,	'2020_05_29_213435_create_transactions_table',	6),
(10,	'2020_06_03_201423_add_columns_to_currencies_table',	7);

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `purses`;
CREATE TABLE `purses` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `purses_user_id_foreign` (`user_id`),
  KEY `purses_currency_id_foreign` (`currency_id`),
  CONSTRAINT `purses_currency_id_foreign` FOREIGN KEY (`currency_id`) REFERENCES `currencies` (`id`),
  CONSTRAINT `purses_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `purses` (`id`, `user_id`, `name`, `currency_id`, `created_at`, `updated_at`) VALUES
(2,	5,	'сбережения',	840,	'2020-06-01 18:48:45',	'2020-06-08 08:49:32'),
(3,	5,	'счет грн.',	980,	'2020-06-02 05:51:10',	'2020-06-02 13:17:10'),
(4,	5,	'счет USD',	840,	'2020-06-02 05:52:07',	'2020-06-02 05:52:07'),
(5,	5,	'счет Евро',	978,	'2020-06-03 15:45:54',	'2020-06-03 15:45:54'),
(6,	2,	'счет грн.',	980,	'2020-06-08 05:28:20',	'2020-06-08 05:28:20');

DROP TABLE IF EXISTS `transactions`;
CREATE TABLE `transactions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `purse_id` bigint(20) unsigned NOT NULL,
  `sum` bigint(20) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `transactions_purse_id_foreign` (`purse_id`),
  CONSTRAINT `transactions_purse_id_foreign` FOREIGN KEY (`purse_id`) REFERENCES `purses` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `transactions` (`id`, `date`, `type`, `purse_id`, `sum`, `description`, `created_at`, `updated_at`) VALUES
(2,	'2020-05-15',	'debet',	3,	12000,	'аванс',	'2020-06-03 15:36:47',	'2020-06-03 15:36:47'),
(6,	'2020-05-30',	'debet',	3,	500,	'зарплата',	'2020-06-03 15:46:38',	'2020-06-04 12:08:47'),
(8,	'2020-06-04',	'debet',	4,	600,	'приход',	'2020-06-06 16:21:24',	'2020-06-07 17:16:30'),
(10,	'2020-06-02',	'debet',	3,	5000,	'приход',	'2020-06-06 16:22:22',	'2020-06-06 16:22:22'),
(15,	'2020-06-02',	'kredit',	3,	-5000,	'расход',	'2020-06-06 16:42:52',	'2020-06-06 16:42:52'),
(16,	'2020-06-01',	'debet',	4,	500,	'приход',	'2020-06-06 16:45:28',	'2020-06-06 16:45:28'),
(18,	'2020-06-01',	'kredit',	4,	-300,	'расход',	'2020-06-06 16:45:55',	'2020-06-06 16:45:55'),
(19,	'2020-06-01',	'debet',	5,	300,	'приход',	'2020-06-06 16:46:12',	'2020-06-06 16:46:12'),
(20,	'2020-06-01',	'kredit',	5,	-200,	'расход',	'2020-06-06 16:46:27',	'2020-06-06 16:46:27'),
(21,	'2020-05-26',	'debet',	4,	299,	'приход',	'2020-06-07 16:53:24',	'2020-06-07 16:53:24'),
(22,	'2020-06-07',	'kredit',	2,	-500,	'расход',	'2020-06-07 17:35:58',	'2020-06-07 17:35:58'),
(28,	'2020-06-03',	'kredit',	3,	2000,	'расход',	'2020-06-07 18:01:31',	'2020-06-08 08:57:54'),
(29,	'2020-06-08',	'kredit',	3,	-2300,	'приход',	'2020-06-08 05:40:43',	'2020-06-08 05:40:43');

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1,	'anna',	'qqq@gmail.com',	NULL,	'$2y$10$ELmPk21irw2eUoYPvbqWtuAdZ8zbdpL3sDdz4tT3BTepibWP/fpFG',	'dCG3XmXXymEXis3bu30B3MC0jeUrC0jcuSP6tlaehBvAj12BehBduLPNlroo',	'2020-05-30 15:07:46',	'2020-06-05 18:05:24'),
(2,	'anna',	'aaa@gmail.com',	NULL,	'$2y$10$r8wbC8I/mqRDXgVj8.yhmuqj1t1K2Xhapub4nZmGx/OTQvVJrvTPa',	'AQymBbGk71Dual5LTvnK9PiKHYSenLMgRoltaDXjae3QObeh4XBdb2EqxOdb',	'2020-06-03 19:41:43',	'2020-06-07 19:29:15'),
(3,	'test',	'test@gmail.com',	NULL,	'$2y$10$40WDFQAgurZs/cL0SMaKSOVBp6reSCdv2D.VJAU4rupu6Kfjm8L3K',	'6v7B1ApD69ovwWgHmJy9x3Lq6PEQP8bMOKWf43WXPMxq5DeS4XOkyp7zOYlt',	'2020-06-07 19:35:48',	'2020-06-08 04:09:13'),
(4,	'222',	'111@gmail.com',	NULL,	'$2y$10$fD22aJfBV3gqTnfMjJXe9.6FWTgRDSoFwI0ck/XoGZZFm495om3TW',	NULL,	'2020-06-08 05:07:12',	'2020-06-08 05:09:50'),
(5,	'222',	'222@gmail.com',	NULL,	'$2y$10$Gi3vMyZ9bub9l.3hTTJNNeZjU3bOWhU59A7Frf1sDtnPQKOQWmZH.',	'AMf37tgmDiXSJvGneacw0vCEyj7e7HtCt8O8XlTNlV8SPw4rStbYMvZIVYKn',	'2020-06-08 05:14:26',	'2020-06-08 08:40:37');

DROP TABLE IF EXISTS `users_settings`;
CREATE TABLE `users_settings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `users_settings_user_id_foreign` (`user_id`),
  CONSTRAINT `users_settings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `users_settings` (`id`, `user_id`, `key`, `value`, `created_at`, `updated_at`) VALUES
(1,	1,	'Валюта баланса',	'840',	NULL,	'2020-06-05 19:02:53'),
(2,	2,	'Валюта баланса',	'840',	'2020-06-06 16:00:58',	'2020-06-07 19:28:47'),
(3,	3,	'Валюта баланса',	'840',	'2020-06-07 20:12:46',	'2020-06-07 20:17:15'),
(4,	3,	'Валюта баланса',	'840',	'2020-06-07 20:16:15',	'2020-06-07 20:17:15'),
(5,	3,	'Валюта баланса',	'840',	'2020-06-07 20:16:57',	'2020-06-07 20:17:15'),
(6,	4,	'Валюта баланса',	'980',	'2020-06-08 05:07:27',	'2020-06-08 05:07:27'),
(7,	5,	'Валюта баланса',	'840',	'2020-06-08 05:14:35',	'2020-06-08 08:51:17');

-- 2020-06-08 11:59:49
