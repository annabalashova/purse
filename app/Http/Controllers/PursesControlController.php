<?php

namespace App\Http\Controllers;

use App\Models\Purse;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PursesControlController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create(Request $request)
    {
        $purse = new Purse();
        $purse->user_id = Auth::user()->id;
        $purse->name = $request->name;
        $purse->currency_id = $request->currency;
        if ($purse->save() && ($request->first_sum != 0 || !empty($request->first_sum))) {
            $this->setFirstSum($request);
            return redirect()->action('PursesViewController@index');
        } else {
            return redirect()->action('PursesViewController@index');
        }
    }

    protected function setFirstSum(Request $request)
    {
        $maxDateCreated = Purse::where('user_id', Auth::id())->max('created_at');
        $purse = Purse::where([
            ['user_id', '=', Auth::id()],
            ['created_at', '=', $maxDateCreated],
        ])->first();
        $transaction = new Transaction();
        $transaction->date = Carbon::now();
        $transaction->type = 'debet';
        $transaction->purse_id = $purse->id;
        $transaction->sum = $request->first_sum;
        $transaction->description = 'установлен начальный баланс';
        $transaction->save();
    }

    public function delete(Request $request)
    {
        $id = $request->purse;
        $purse = Purse::where('id', $id)->first();
        if (!$purse->transactions->all()) {
            $purse->delete();
            return redirect()->action('PursesViewController@index');
        } else {
            echo 'При наличии транзакций кошелек не может быть удален' . '<br>';
            echo "<a href='http://purse.loc/transactions'>Перейти на страницу транзакций</a>";
        }
    }

    public function update(Purse $purse, Request $request)
    {
        $purse->name = $request->name;
        if ($purse->save()) {
            return redirect()->action('PursesViewController@index');
        }
    }

}
