<?php

namespace App\Http\Controllers;

use App\Models\Currency;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $currencies = Currency::all();
        return view('update_user', ['currencies' => $currencies]);
    }

    public function update(User $user, Request $request)
    {
        $user->name = $request->name;
        if (!empty($request->password) && $request->password === $request->password_confirmation) {
            $user->password = Hash::make($request->password);
        }
        if ($user->save()) {
            return redirect()->action('HomeController@index');
        }
    }

}
