<?php

namespace App\Http\Controllers;

use App\Models\Currency;
use App\Models\Purse;
use App\Models\Transaction;
use App\Models\UsersSetting;
use App\RateService\RateServiceInterfase;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class TransactionsViewController extends Controller

{
    public $mainRate;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(RateServiceInterfase $api)
    {
      //  $api = app()->get('apiRate');
        $rates = $api->getRates();
        $mainRate = $api->getMainRate();

        $transactions = Transaction::select('transactions.*', 'purses.name', 'currencies.ccy')->leftJoin('purses', 'purses.id', '=', 'transactions.purse_id')
            ->leftJoin('currencies', 'currencies.id', '=', 'purses.currency_id')
            ->where('user_id', Auth::user()->id)->latest('date')->simplePaginate(3);
        return view('transaction', ['transactions' => $transactions, 'rates' => $rates, 'mainRate' => $mainRate]);
    }

    public function showCreate()
    {
        $purses = Purse::where('user_id', Auth::user()->id)->get();
        return view('create_transaction', ['purses' => $purses]);
    }

    public function showUpdate(Transaction $transaction, Request $request)
    {
        $purses = Purse::where('user_id', Auth::user()->id)->get();
        return view('update_transaction', ['transaction' => $transaction, 'purses' => $purses]);
    }

}
