<?php

namespace App\Http\Controllers;

use App\Models\Purse;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TransactionsControlController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create(Request $request)
    {
        $transaction = new Transaction();
        $transaction->date = $request->date;
        $transaction->type = $request->type;
        $transaction->purse_id = $request->purse;
        $transaction->sum = ($request->type == "kredit") ? '-' . $request->sum : $request->sum;
        $transaction->description = $request->description;
        if ($transaction->save()) {
            return redirect()->action('TransactionsViewController@index');
        }
    }

    public function delete(Transaction $transaction)
    {
        if ($transaction->delete()) {
            return redirect()->action('TransactionsViewController@index');
        }
    }

    public function update(Transaction $transaction, Request $request)
    {
        $transaction->date = $request->date;
        $transaction->type = $request->type;
        $transaction->purse_id = $request->purse;
        $transaction->sum = $request->sum;
        $transaction->description = $request->description;
        if ($transaction->save()) {
            return redirect()->action('TransactionsViewController@index');

        }
    }

}

