<?php

namespace App\Http\Controllers;

use App\Models\Currency;
use App\Models\Purse;
use App\Models\UsersSetting;
use App\RateService\RateServiceInterfase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index(RateServiceInterfase $api)
    {
        $purses = Purse::where('user_id', Auth::user()->id)->get();

       // $api = app()->get('apiRate');
        $rates = $api->getRates();
        $mainRate = $api->getMainRate();

        $allSum = 0;
        foreach ($purses as $purse) {
            $allSum += $purse->transactions()->sum('sum') * $rates[$purse->currency->ccy];
        }

        $allSum = round($allSum / $mainRate, 2);

        $userSetting = UsersSetting::where([
                ['user_id', '=', Auth::user()->id],
                ['key', '=', 'Валюта баланса'],
            ])->first();

       return view('home', ['purses' => $purses, 'userSetting' => $userSetting], ['allSum' => $allSum ]);
    }

}
