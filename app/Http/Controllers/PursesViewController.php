<?php

namespace App\Http\Controllers;

use App\Models\Currency;
use App\Models\Purse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PursesViewController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('purses');
    }

    public function showCreate()
    {
        $currencies = Currency::all();
        return view('create_purse', ['currencies' => $currencies]);
    }

    public function select()
    {
        $purses = Purse::where('user_id', Auth::user()->id)->pluck('name', 'id');
        return view('select_purse', ['purses' => $purses]);
    }

    public function showUpdate(Request $request)
    {
        $purse = Purse::where('id', $request->purse)->first();
        return view('update_purse', ['model' => $purse]);
    }

    public function showDelete()
    {
        $purses = Purse::where('user_id', Auth::user()->id)->pluck('name', 'id');
        return view('delete_purse', ['purses' => $purses]);
    }

}
