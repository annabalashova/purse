<?php

namespace App\Http\Controllers;

use App\Models\Currency;
use App\Models\UsersSetting;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;

class SettingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $currencies = Currency::all();
        return view('setting', ['currencies' => $currencies]);
    }

    public function create(Request $request)
    {
        $usersSetting = new UsersSetting();
        $usersSetting->user_id = Auth::user()->id;
        $usersSetting->key = $request->key;
        $usersSetting->value = $request->currency;
        if ($usersSetting->save()) {
            return redirect()->action('HomeController@index');;
        }
    }

    public function update(Request $request)
    {
        $model = UsersSetting::where([
            ['user_id', '=', Auth::user()->id],
            ['key', '=', $request->key]
        ])->update(['value' => $request->currency]);
        if ($model) {
            return redirect()->action('HomeController@index');
        }
    }

}
