<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Purse extends Model
{
    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

}
