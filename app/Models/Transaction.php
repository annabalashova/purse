<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    public function getSumAttribute($value)
    {
        return abs($value);
    }

    public function purse()
    {
        return $this->belongsTo(Purse::class);
    }

    public function getTypeAttribute($value)
    {
        if ($value == 'debet') {
            return 'приход';
        } elseif ($value == 'kredit') {
            return 'pacход';
        }
    }

}
