<?php

namespace App\Providers;

use App\RateService\ApiRate;
use App\RateService\RateServiceInterfase;
use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;

class RateServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('client', function ($app) {
            return new Client();
        });
        $this->app->bind(RateServiceInterfase::class, function ($app) {
            return new ApiRate(app()->make('client'));
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
