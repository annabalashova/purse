<?php


namespace App\RateService;


interface RateServiceInterfase
{
    public function getMainRate();
    public function getRates();
}
