<?php


namespace App\RateService;


//use App\Models\Image;
use App\Models\Currency;
use App\Models\UsersSetting;
use GuzzleHttp\ClientInterface;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class ApiRate implements RateServiceInterfase
{
    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    protected function getResponse()
    {
        $response = $this->client->request(
            'GET',
            "https://api.privatbank.ua/p24api/pubinfo?exchange&json&coursid=11");
        return $response;
    }

    public function getRates()
    {
        $rates = [];

        if (!Cache::has('USD')) {
            $response = json_decode($this->getResponse()->getBody(), true, 512);
            foreach ($response as $item) {
                Cache::put($item['ccy'], $item['sale'], Carbon::now()->addMinutes(1));
                $rates[$item['ccy']] = Cache::get($item['ccy']);
            }
        } else {
            $currencies = Currency::pluck('ccy');
            $ccy = $currencies->toArray();
            foreach ($ccy as $item) {
                $rates[$item] = Cache::get($item);
            }
        }
        $rates['UAH'] = 1;
        return $rates;
    }

    public function getMainRate()
    {
        $model = UsersSetting::leftJoin('currencies', 'currencies.id', '=', 'users_settings.value')
            ->where('user_id', Auth::user()->id)->first();
        $rates = $this->getRates();
        $mainRate = 1;
        foreach ($rates as $key => $rate) {
            if (isset($model->ccy) && $model->ccy == $key) {
                $mainRate = $rate;
            }
        }
        return $mainRate;
    }

}
