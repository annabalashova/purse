@extends('layouts.app')

@section('content')

    <div>
        <h3>Введите данные для редактирования транзакции</h3>
        <form method="post" action="/transactions-control/update/{{ $transaction->id }}" autocomplete="on">
            {{ csrf_field() }}
            <label for="date">Дата транзакции</label>
            <br>
            <input type="date" id="date" name="date" value={{  $transaction->date }}>
            <br> <br>
            <lablel>Тип транзакции</lablel>
            <br>
            <select name="type">
                <option value="0" selected>Выберите тип транзакции</option>
                <option value="debet">приход</option>
                <option value="kredit">расход</option>
            </select> <br><br>
            <lablel>Выберите кошелек</lablel>
            <br>
            <select name="purse">
                <option value="0" selected>Выберите кошелек из списка</option>
                @foreach($purses as $id => $purse)
                    <option value="{{ $purse->id }}">{{ $purse->name }}</option>
                @endforeach
            </select> <br><br>
            <lablel>Сумма</lablel>
            <br>
            <input name="sum" value={{$transaction->sum}}>
            <br> <br>
            <lablel>Описание</lablel>
            <br>
            <input name="description" value={{$transaction->description}}>
            <br> <br>
            <button type="submit" name="upload">Сохранить</button>
        </form>
    </div>

@endsection

