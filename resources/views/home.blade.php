@extends('layouts.app')

@section('content')
    <div>
        <p>
            Вы вошли как {{ Auth::user()->name }}
            <br><br>
            email:{{ Auth::user()->email }}
        </p>
        <p><a href="user">Изменить личные данные</a></p>
        <h3>Основные настройки:</h3>
        <p>
            Валюта баланса:
            @if(isset( $userSetting->value))
                {{ $userSetting->currency->name }}
                <br>
                <a href="user">Изменить валюту баланса </a>
        </p>
        <h3>Список кошельков:</h3>
        <table border="0">
            <tr>
                <th>Название кошелька</th>
                <th>Баланс</th>
                <th>Валюта</th>
            </tr>
            @foreach($purses as $purse)
                <tr>
                    <td>{{ $purse->name }}</td>
                    <td>{{ $purse->transactions()->sum('sum')}}</td>
                    <td>{{ $purse->currency->name}}</td>
                </tr>
            @endforeach
        </table>
        <p>
            <br>
            <a href="purses">Управление кошельками</a>
        </p>
        <h3>Сумма баланса :</h3>
        <p>
            на сегодня составляет: {{$allSum}} в валюте {{ $userSetting->currency->name }}
        </p>
        @else
            <p>
                Для дальнейшей работы необходимо установить валюту баланса в настройках приложения.
            </p>
        @endif
    </div>

@endsection

