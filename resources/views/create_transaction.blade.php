@extends('layouts.app')

@section('content')

    <div>
        <h3>Введите данные для создания транзакции</h3>
        <form method="post" action="/transactions-control/create" autocomplete="on">
            {{ csrf_field() }}
            <label for="date">Дата транзакции</label>
            <br>
            <input type="date" id="date" name="date">
            <br> <br>
            <lablel>Тип транзакции</lablel>
            <br>
            <select name="type">
                <option value="0" selected>Выберите тип транзакции</option>
                <option value="debet">приход</option>
                <option value="kredit">расход</option>
            </select> <br><br>
            <lablel>Кошелек</lablel>
            <br>
            <select name="purse">
                <option value="0" selected>Выберите кошелек из списка</option>
                @foreach($purses as $id => $purse)
                    <option value="{{ $purse->id }}">{{ $purse->name }}</option>
                @endforeach
            </select> <br><br>
            <lablel>Сумма</lablel>
            <br>
            <input name="sum" value="">
            <br> <br>
            <lablel>Описание</lablel>
            <br>
            <textarea rows="10" cols="36" name="description"></textarea>
            <br> <br>
            <button type="submit" name="upload">Сохранить</button>
        </form>
    </div>

@endsection
