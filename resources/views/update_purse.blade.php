@extends('layouts.app')

@section('content')

    <div>
        <h3>Введите данные для редактирования кошелька</h3>
        <form method="post" action="http://purse.loc/purses-control/update/{{ $model->id }}" autocomplete="on">
            {{ csrf_field() }}
            <lablel>Название</lablel>
            <br>
            <input name="name" value={{ $model->name }}>
            <br>
            <lablel>Валюта кошелька</lablel>
            <br>
            <input name="currency" value={{ $model->currency->name }} disabled>
            <button type="submit" name="upload">Сохранить</button>
        </form>
    </div>

@endsection
