@extends('layouts.app')

@section('content')

    <div>
        <form method="post" action="/user/update/{{ Auth::user()->id }}" autocomplete="on">
            {{ csrf_field() }}
            <h3>Изменить личные данные</h3>
            <lablel>Имя</lablel>
            <br>
            <input name="name" value="{{ Auth::user()->name }}">
            <br>
            <lablel>Изменить пароль</lablel>
            <br>
            <input type="password" name="password" pattern=".{8,}"
                   title="Минимальная длина пароля должна быть от 8 символов">
            <br>
            <lablel>Введите пароль повторно</lablel>
            <br>
            <input type="password" name="password_confirmation" pattern=".{8,}"
                   title="Минимальная длина пароля должна быть от 8 символов">
            <br><br>
            <button type="submit" name="upload">Сохранить</button>
            <br> <br>
        </form>
        <form method="post" action="/setting/update" autocomplete="on">
            {{ csrf_field() }}
            <h3>Изменение настроек</h3>
            <lablel>Название</lablel>
            <br>
            <input name="key" value="Валюта баланса" readonly>
            <br><br>
            <select name="currency">
                <option value="0" selected>Выберите валюту из списка</option>
                @foreach($currencies as $currency)
                    <option value="{{ $currency->id }}">{{ $currency->name }}</option>
                @endforeach
            </select>
            <br><br>
            <button type="submit" name="upload">Сохранить</button>
        </form>
    </div>

@endsection

