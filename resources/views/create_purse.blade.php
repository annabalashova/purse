@extends('layouts.app')

@section('content')

    <div>
        <h3>Введите данные для создания нового кошелька</h3>
        <form method="post" action="/purses-control/create" autocomplete="on">
            {{ csrf_field() }}
            <lablel>Название</lablel>
            <br>
            <input name="name" value="">
            <br>
            <lablel>Валюта кошелька</lablel>
            <br>
            <select name="currency">
                <option value="0" selected>Выберите валюту из списка</option>
                @foreach($currencies as $currency)
                    <option value="{{ $currency->id }}">{{ $currency->name }}</option>
                @endforeach
            </select> <br><br>
            <lablel>Начальный баланс(при  наличии)</lablel>
            <br>
            <input name="first_sum" value="0">
            <br>
            <button type="submit" name="upload">Сохранить</button>
        </form>
    </div>
@endsection
