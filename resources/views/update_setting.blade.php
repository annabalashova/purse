@extends('layouts.app')

@section('content')

    <div>
        <form method="post" action="/setting/update" autocomplete="on">
            {{ csrf_field() }}
            <h3>Изменение настроек</h3>
            <lablel>Название</lablel>
            <br>
            <input name="key" value="Валюта баланса" readonly>
            <br><br>
            <select name="value">
                <option value="0" selected>Выберите валюту из списка</option>
                @foreach($currencies as $currency)
                    <option value="{{ $currency->id }}">{{ $currency->name }}</option>
                @endforeach
            </select> <br><br>
            <button type="submit" name="upload">Сохранить</button>
        </form>
    </div>

@endsection
