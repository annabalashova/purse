@extends('layouts.app')

@section('content')

    <div>
        <h3>Выберите кошелек для удаления </h3>
        <form method="post" action="http://purse.loc/purses-control/delete" autocomplete="on">
            {{ csrf_field() }}
            <select name="purse">
                <option value="0" selected>Выберите кошелек из списка</option>
                @foreach($purses as $id => $purse)
                    <option value="{{ $id }}">{{ $purse }}</option>
                @endforeach
            </select> <br><br>
            <button type="submit" name="upload">Выбрать</button>
        </form>
    </div>

@endsection
