@extends('layouts.app')

@section('content')
    <div>
        <p>
            <a href="http://purse.loc/transactions/create">Добавить транзакцию</a>
        </p>
        <h3>Список транзакций:</h3>
        <table border="0">
            <tr>
                <th>Дата транзакции</th>
                <th>Тип транзакции</th>
                <th>Кошелек</th>
                <th>Сумма в валюте кошелька</th>
                <th>Сумма в основной валюте</th>
                <th>Описание</th>
                <th>Удалить</th>
                <th>Редактировать</th>
            </tr>
            @foreach($transactions as  $transaction)
                <tr>
                    <td>{{  $transaction->date }}</td>
                    <td>{{  $transaction->type }}</td>
                    <td>{{  $transaction->name }}</td>
                    <td>{{  $transaction->sum  }}</td>
                    <td>{{ round($transaction->sum * $rates[$transaction->ccy] / $mainRate, 2 )}}</td>
                    <td>{{  $transaction->description }}</td>
                    <td><a href="http://purse.loc/transactions-control/delete/{{$transaction->id}}">Удалить</a></td>
                    <td><a href="http://purse.loc/transactions/update/{{$transaction->id}}">Редактировать</a></td>
                </tr>
            @endforeach
        </table>
    </div>
    {{ $transactions->links() }}

@endsection

